**Note:** In the time since this package was written, NiceHash has released
[NHOS](https://github.com/nicehash/NHOS), a minimal Linux system you can
write to a USB stick and quickly configure to bring up a new GPU or ASIC
miner.  It's free to use on their platform for as many mining rigs as you
want, it's actively maintained, and it's probably a better option at this 
point than my switcher if you're using NiceHash.

nvOC NiceHash Switcher
======================

nvOC makes it easy to squeeze more hashrate out of your GPUs.  NiceHash
makes it easy to mine altcoins and get paid in Bitcoin.  However, nvOC is a
customized Ubuntu Linux and the mining software NiceHash provides to mine
the most profitable coin only runs on Windows.

This set of scripts bridges the gap.  With them, you can use the miners
included in nvOC to mine whatever's most profitable at NiceHash.  For each
supported algorithm, you can customize power, clockspeed, and fan speed
settings as you consider appropriate.  Power costs are factored into
profitability calculations as well, and if no enabled coin is profitable,
your rig will be idled until something is profitable.

Setup
-----

1. ssh into your mining rig's m1 account.
2. Clone these files into the same directory as nvOC's oneBash script.
3. Disable oneBash by commenting out all lines in ~/2unix.  oneBash should
be invoked manually if you add/remove GPUs so it can reconfigure
/etc/X11/xorg.conf, but it otherwise isn't used.  To keep gnome-terminal
from sucking down CPU cycles by constantly restarting the 2unix script, you
might want to include something like "sleep 86400" at the end.
4. Edit conf.json.  You'll set your power cost and the mining rates and power
consumption of various algorithms here.  You can extend the performance list
to include additional mining algorithms supported by both NiceHash and the
miners included with nvOC.  Your Bitcoin address and rig name also go here...
though if you want to mine to my tipjar, I won't complain. :-) Comments as
to what's what in conf.json can still be found at the top of switch.py.
5. If your network is IPv6-enabled, note that nvOC (as of v17) has a problem
connecting to IPv6 hosts.  NiceHash makes its API available over both IPv6
and IPv4, so to force it to use IPv4 in the meantime, edit /etc/hosts as
follows:

    `104.20.158.21 api.nicehash.com`
6. Set switch.py to launch with a cronjob:

    `0,10,20,30,40,50 * * * * (cd /media/m1/1263-A96E && python2.7 switch.py conf.json)`

Every 10 minutes (which is how often NiceHash recalculates profitability),
the script will run and determine which miner needs to start up.  If it's
already running, it will leave it alone.  If it needs to switch, it kills
one miner and starts the other.  If nothing's running (because the rig just
rebooted or the miner crashed), it will start the miner.

current-profit is a text file that will be updated on every run of switch.py
to show which is currently the most profitable.  It provides profit values
in BTC and your local currency.

algo-log is a list of algorithms the switcher has selected when run.  If you
start seeing repeats of the same algo every 10 minutes, that's a sign that
something has gone wrong with your GPUs and you might want to consider a
reboot.

The power and hashrate numbers in the stock configuration are for my rig,
which currently uses three GeForce GTX 1070s (one reference-design, the
others a bit factory-overclocked and with massive coolers) on an Asus Prime
Z270-AR, driven by a Thermaltake 650W 80+ Bronze power supply.  Wall power
is measured with a Kill-a-Watt.  Some miners (such as the EWBF miner) show
power usage at the card.  I'm in the middle of recalculating the power-
consumption numbers, so perhaps the ones included in earlier commits would
be more useful for now.  

Miners are started in Screen sessions (http://www.gnu.org/software/screen)
that can be viewed over an ssh login session:

`screen -dr miner`

When the rig switches algorithms, the Screen session will be closed and a
new one opened.  Repeat the above to monitor it, or you could use something
like this on a monitoring workstation:

`while true; do screen -dr miner; done`

Adding Algorithms
-----------------

Some algorithms supported by NiceHash can't be mined with the miners
included in nvOC.  If you add the appropriate software, though, you can mine
these as well.  Instructions to add these capabilities will follow.

Sia
---

To add the miner:

`sudo apt install golang-go
mkdir /home/m1/gocode
GOPATH=/home/m1/gocode go get github.com/robvanmieghem/gominer`

To enable mining, create a new object named "sia" within the "performance"
list in conf.json.  Here's what I use:

```
 "performance": 
 {
   [existing entries deleted for brevity]
   "sia": {
     "speed": 3.700, "power": 0.403,
     "power_limit": [115,115,95], "gpu_oc": [-100,-100,0], "mem_oc": 0, "fan": [70,75,70],
     "cmd": "/home/m1/gocode/bin/gominer -url stratum+tcp://sia.usa.nicehash.com:3360 -user {ADDR}.{MINER}"
   }
 }
```

